plugins {
    kotlin("jvm")
    kotlin("kapt")
    kotlin("plugin.noarg")
    kotlin("plugin.spring")
    id("com.vanniktech.maven.publish")
}

group = "cloud.hedou"
version = "1.2.6"
description = "适用于新版基础数据(43111)的版本"

java.sourceCompatibility = JavaVersion.VERSION_1_8

@Suppress("GradlePackageUpdate")
dependencies {
    // kotlin
    implementation(kotlin("stdlib"))
    // http
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.retrofit2:converter-jackson:2.9.0")
    implementation("com.squareup.okhttp3:logging-interceptor:3.14.9")
    // caches
    implementation("com.github.ben-manes.caffeine:caffeine:2.9.3")
    implementation("org.springframework:spring-context-support:5.3.18")
    // rabbitMQ
    api("org.springframework.amqp:spring-rabbit:2.4.3")
    // oauth jwks
    implementation("org.springframework.boot:spring-boot-starter-security:2.6.6")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server:2.6.6")
    // spring boot
    compileOnly("org.springframework.boot:spring-boot-starter-web:2.6.6")
    compileOnly("org.springframework.data:spring-data-jpa:2.6.3")
    compileOnly("org.hibernate:hibernate-core:5.6.7.Final")
    // configuration processor
    kapt("org.springframework.boot:spring-boot-configuration-processor:2.6.6")
    // tests
    testImplementation("org.junit.jupiter:junit-jupiter:5.8.1")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

noArg {
    annotations("cloud.hedou.annotation.NoArguments")
}

mavenPublish {
    sonatypeHost = com.vanniktech.maven.publish.SonatypeHost.S01
}
