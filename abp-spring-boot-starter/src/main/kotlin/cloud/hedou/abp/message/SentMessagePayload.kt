package cloud.hedou.abp.message

import com.fasterxml.jackson.annotation.JsonProperty

data class SentMessagePayload(

    /** 消息频道名称 */
    @get:JsonProperty("notificationName")
    var channelName: String,

    /** 消息体 */
    var data: MessageData,

    /** 严重性 */
    var severity: Int,

    /** 接收此消息的用户ID */
    var userIds: List<MessageReceiver>

)