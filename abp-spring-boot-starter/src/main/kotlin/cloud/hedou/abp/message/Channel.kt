package cloud.hedou.abp.message

import cloud.hedou.annotation.NoArguments

@NoArguments
data class Channel(

    /** 频道类型 */
    var type: Int,

    /** 频道唯一标识 */
    var name: String,

    /** 频道名称 */
    var displayName: String,

    /** 频道分组 */
    var group: String,

    /** 频道分组名称 */
    var groupDisplay: String,

    /** 描述 */
    var description: String?,

    /** 可用的通知器 */
    var allowedNotifiers: List<Int>?,

    /** 额外属性 */
    var extraProperties: Map<String, String>? = mapOf("NotificationDefinitionKey" to "__NotificationDefinitionKey"),

    ) {

    companion object {

        /** 系统消息 */
        const val TYPE_SYSTEM = 0

        /** 业务消息 */
        const val TYPE_BUSINESS = 1

        /** 消息 */
        const val NOTIFIER_MESSAGE = 0

        /** 邮件 */
        const val NOTIFIER_EMAIL = 1

        /** 短信 */
        const val NOTIFIER_SMS = 2

        /** TPNS */
        const val NOTIFIER_TPNS = 3

        /** 短信猫 */
        const val NOTIFIER_SMS_CAT = 20

    }

}