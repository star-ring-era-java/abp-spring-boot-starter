package cloud.hedou.abp.message

import cloud.hedou.annotation.NoArguments

@NoArguments
data class MessageData(

    /** 消息标题 */
    var title: String,

    /** 消息内容 */
    var message: String,

    /** 额外属性 */
    var properties: Map<String, Any>?

)