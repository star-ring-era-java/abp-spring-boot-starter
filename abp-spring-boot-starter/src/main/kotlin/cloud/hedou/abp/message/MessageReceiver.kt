package cloud.hedou.abp.message

import cloud.hedou.annotation.NoArguments

@NoArguments
data class MessageReceiver(

    var tenantId: String,

    var userId: String

)