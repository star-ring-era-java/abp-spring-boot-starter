package cloud.hedou.abp.message

import cloud.hedou.abp.identity.RemoteIdentityService
import cloud.hedou.abp.starter.AbpContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Component
@Order(Int.MIN_VALUE)
class IdentityRegisterRunner : CommandLineRunner {

    @Autowired
    private lateinit var remoteIdentityService: RemoteIdentityService

    override fun run(vararg args: String?) {
        // 注册服务器，获得token
        AbpContext.serverToken = remoteIdentityService.register()
    }

}