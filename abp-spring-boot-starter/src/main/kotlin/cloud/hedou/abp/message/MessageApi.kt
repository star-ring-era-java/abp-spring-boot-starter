package cloud.hedou.abp.message

import cloud.hedou.abp.remote.Api
import cloud.hedou.abp.remote.PagedList
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

@Api(44386)
interface MessageApi {

    /** 发送消息 */
    @POST("api/hedou/notifications/send")
    fun sendMessage(@Header("Authorization") token: String, @Body payload: SentMessagePayload): Call<Unit?>

    /** 创建消息频道 */
    @POST("api/hedou/notifications/batch")
    fun createChannel(@Header("Authorization") token: String, @Body payload: PagedList<Channel>): Call<Unit?>

}