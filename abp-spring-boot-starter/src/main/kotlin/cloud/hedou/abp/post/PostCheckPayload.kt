package cloud.hedou.abp.post

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonProperty

@NoArguments
data class PostCheckPayload(

    /** 业务名称 */
    val businessName: String,

    /** 用户ID */
    val userId: String,

    /** 部门ID */
    @get:JsonProperty("organizationUnitId")
    val departmentId: String?,

    /** 是否包含子部门 */
    @get:JsonProperty("containesChildren")
    val includeChildren: Boolean?,

    )