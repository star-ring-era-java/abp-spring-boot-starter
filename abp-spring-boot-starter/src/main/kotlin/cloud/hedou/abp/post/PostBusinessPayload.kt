package cloud.hedou.abp.post

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonProperty

@NoArguments
data class PostBusinessPayload(

    @get:JsonProperty("businessName")
    val name: String,

    val display: String,

    val description: String?

)