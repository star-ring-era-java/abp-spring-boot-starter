package cloud.hedou.abp.post

import cloud.hedou.abp.remote.Api
import cloud.hedou.abp.remote.PagedList
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

@Api(43590)
interface PostApi {

    @POST("api/position-management/position/business")
    fun createBusiness(@Body payload: PagedList<PostBusinessPayload>): Call<Unit?>

    @POST("api/position-management/data-control/check")
    fun check(@Body payload: PostCheckPayload): Call<Unit?>

    @POST("api/position-management/data-control/has-permission")
    fun hasPermission(@Body payload: PostCheckPayload): Call<Boolean>

}