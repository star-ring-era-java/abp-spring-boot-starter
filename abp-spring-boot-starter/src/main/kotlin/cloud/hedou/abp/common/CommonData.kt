package cloud.hedou.abp.common

import cloud.hedou.annotation.Identifiable
import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@NoArguments
data class CommonData(

    var name: String,

    var value: String,

    var parentId: String?,

    var color: String?,

    var category: String?,

    var sort: Int,

    var description: String?,

    @Identifiable
    var code: String?,

    @Identifiable
    var id: String

) : Serializable {

    @set:JsonAlias("organizationUnits")
    @get:JsonProperty("organizationUnits")
    var departmentIdList: List<String>? = null

    var children: List<CommonData>? = null

    override fun toString(): String {
        return name
    }

    companion object {
        private const val serialVersionUID = -8002702236781254819L
    }

}
