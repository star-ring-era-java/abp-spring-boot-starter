package cloud.hedou.abp.common

import cloud.hedou.abp.identity.AbpUserDefaults
import cloud.hedou.abp.remote.Api
import cloud.hedou.abp.remote.PagedList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

@Api(43111)
interface RemoteCommonApi {

    @GET("api/basals/base-data")
    fun getCommonData(
        @Query("SkipCount") skipCount: Int,
        @Query("MaxResultCount") resultCount: Int,
        @Query("Category") category: String? = null,
    ): Call<PagedList<CommonData>>

    @GET("api/basals/base-data/{id}")
    fun getCommonDataById(@Path("id") id: String): Call<CommonData>

    @GET("api/basals/user-configuration/get-by-userId")
    fun getUserDefaults(@Query("userId") userId: String): Call<AbpUserDefaults>

}