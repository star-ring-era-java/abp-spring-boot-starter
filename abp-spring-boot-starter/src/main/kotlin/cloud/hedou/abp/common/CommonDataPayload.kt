package cloud.hedou.abp.common

data class CommonDataPayload(
        val skipCount: Long,
        val maxResultCount: Long,
        val category: String? = null,
        val isGroup: Boolean? = null,
)