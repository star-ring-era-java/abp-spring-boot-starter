package cloud.hedou.abp.identity

import cloud.hedou.annotation.NoArguments

@NoArguments
data class UserRabbitPayload(var entity: AbpUser)