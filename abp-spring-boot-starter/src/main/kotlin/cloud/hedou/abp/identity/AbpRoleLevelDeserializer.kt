package cloud.hedou.abp.identity

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.node.ObjectNode

class AbpRoleLevelDeserializer : StdDeserializer<Int>(Int::class.javaPrimitiveType) {

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Int {
        val objectNode = p.readValueAsTree<ObjectNode>()
        val jsonNode = objectNode?.get("Level")
        if(jsonNode?.isInt == true) {
            return jsonNode.intValue()
        } else if(jsonNode?.isTextual == true) {
            return jsonNode.textValue().toInt()
        }
        return 0
    }

}