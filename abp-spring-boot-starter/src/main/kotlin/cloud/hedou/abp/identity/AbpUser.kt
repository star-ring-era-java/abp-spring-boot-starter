package cloud.hedou.abp.identity

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@NoArguments
data class AbpUser(

    var id: String,

    @get:JsonAlias("userName")
    @set:JsonProperty("userName")
    var username: String,

    var tenantId: String? = null,

    var surname: String? = null,

    var name: String? = null,

    var email: String? = null,

    var phoneNumber: String? = null

) : Serializable {

    /** 用户拥有的权限 */
    var roles: List<AbpRole>? = null

    override fun toString(): String {
        return buildString {
            if (!surname.isNullOrEmpty()) {
                append(surname)
            }
            if (!name.isNullOrEmpty()) {
                append(name)
            }
            if (isEmpty()) {
                append(username)
            }
        }
    }

    companion object {
        private const val serialVersionUID = -14L
    }

}