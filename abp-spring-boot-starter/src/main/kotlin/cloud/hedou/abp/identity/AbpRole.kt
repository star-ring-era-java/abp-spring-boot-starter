package cloud.hedou.abp.identity

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.io.Serializable

@NoArguments
data class AbpRole(

    var id: String,

    var name: String,

    @set:JsonAlias("extraProperties")
    @set:JsonDeserialize(using = AbpRoleLevelDeserializer::class)
    var level: Int

): Serializable {

    override fun toString(): String = name

    companion object {
        private const  val serialVersionUID = -126L
    }

}
