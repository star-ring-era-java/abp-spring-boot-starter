package cloud.hedou.abp.identity

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonAlias

@NoArguments
data class AbpAuthToken(

    @set:JsonAlias("access_token")
    var accessToken: String

)