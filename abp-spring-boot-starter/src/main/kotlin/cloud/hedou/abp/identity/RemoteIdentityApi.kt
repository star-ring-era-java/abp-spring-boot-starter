package cloud.hedou.abp.identity

import cloud.hedou.abp.remote.Api
import cloud.hedou.abp.remote.PagedList
import cloud.hedou.abp.remote.UserConfiguration
import retrofit2.Call
import retrofit2.http.*

@Api(44318)
interface RemoteIdentityApi {

    /** 注册此服务，获取专用的token */
    @FormUrlEncoded
    @POST("connect/token")
    fun register(@FieldMap form: Map<String, String?>): Call<AbpAuthToken>

    /** 根据部门与角色搜索用户 */
    @GET("api/identity/users")
    fun findUsers(
        @Query("RoleId") roleId: String?,
        @Query("OrganizationUnitId") departmentId: String?,
        @Query("SkipCount") skipCount: Int,
        @Query("MaxResultCount") resultCount: Int,
        @Query("NotActive") isActive: Boolean = false,
    ): Call<PagedList<AbpUser>>

    @GET("api/identity/roles")
    fun getRoles(
        @Query("SkipCount") skip: Int,
        @Query("MaxResultCount") count: Int,
    ): Call<PagedList<AbpRole>>

    /** 通过用户ID查询用户信息 */
    @GET("api/identity/users/{id}")
    fun getUserById(@Path("id") userId: String): Call<AbpUser>

    /** 获取指定用户所属的部门 */
    @GET("api/identity/organization-units/{id}")
    fun getDepartmentById(@Path("id") departmentId: String): Call<AbpDepartment>

    /** 获取指定用户所属的部门 */
    @GET("api/identity/users/{id}/organization-units")
    fun getDepartmentsByUserId(@Path("id") userId: String): Call<List<AbpDepartment>>

    /** 获取指定用户所属的部门 */
    @GET("api/identity/organization-units")
    fun getDepartments(
        @Query("SkipCount") skipCount: Int,
        @Query("MaxResultCount") maxResultCount: Int,
    ): Call<PagedList<AbpDepartment>>

    /** 获取某个用户的角色列表 */
    @GET("api/identity/users/{id}/roles")
    fun getRoleByUserId(@Path("id") userId: String): Call<PagedList<AbpRole>>

    /** 获取指定部门下的角色列表 */
    @GET("api/identity/organization-units/{id}/roles")
    fun getRolesByDepartmentId(
        @Path("id") departmentId: String,
        @Query("SkipCount") skip: Int,
        @Query("MaxResultCount") count: Int,
    ): Call<PagedList<AbpRole>>

    /** 获取指定部门下的用户列表 */
    @GET("api/identity/organization-units/{id}/members")
    fun getUsersByDepartmentId(
        @Path("id") departmentId: String,
        @Query("SkipCount") skip: Int,
        @Query("MaxResultCount") count: Int,
    ): Call<PagedList<AbpUser>>

    /** 获取指定部门下的用户列表，包含用户所属的角色 */
    @GET("api/OrganizationUnits/HdOrganizationUnitLookup/lookup/organization-units-with-users")
    fun getUserWithRolesByDepartmentId(
        @Query("OrganizationUnitId") departmentId: String,
        @Query("SkipCount") skip: Int,
        @Query("MaxResultCount") count: Int,
    ): Call<PagedList<AbpDepartmentWithUsers>>

    /** 获取所有的权限列表 */
    @GET("api/abp/application-configuration")
    fun getUserConfiguration(): Call<UserConfiguration>

}