package cloud.hedou.abp.identity

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@NoArguments
data class AbpDepartment(

    var id: String,

    @set:JsonProperty("displayName")
    var name: String,

    var parentId: String?

) : Serializable {

    override fun toString(): String = name

    companion object {
        private const  val serialVersionUID = -15505L
    }

}