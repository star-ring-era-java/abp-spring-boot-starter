package cloud.hedou.abp.identity

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonProperty

@NoArguments
data class AbpUserDefaults(

    /** 用户ID */
    var userId: String,

    /** 角色ID */
    @set:JsonProperty("businessRoleId")
    var roleId: String?,

    /** 部门ID */
    @set:JsonProperty("businessOrganizationUnitId")
    var departmentId: String?

)