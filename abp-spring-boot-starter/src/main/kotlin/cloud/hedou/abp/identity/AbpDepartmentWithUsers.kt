package cloud.hedou.abp.identity

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonProperty

@NoArguments
data class AbpDepartmentWithUsers(

    @set:JsonProperty("organizationUnits")
    var department: AbpDepartment,

    var users: List<AbpUser>

)