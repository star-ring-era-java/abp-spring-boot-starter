package cloud.hedou.abp.extension

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper

inline fun <reified T> String.deserialize(objectMapper: ObjectMapper): T {
    return objectMapper.readValue(this, object : TypeReference<T>() {})
}

inline fun <reified T> T.serialize(objectMapper: ObjectMapper): String {
    return objectMapper.writeValueAsString(this)
}
