package cloud.hedou.abp.schedule

import cloud.hedou.abp.remote.HttpClient
import cloud.hedou.abp.remote.HttpClient.Companion.create
import org.springframework.stereotype.Service
import retrofit2.HttpException

@Service
class RemoteScheduleService(httpClient: HttpClient) {

    private val scheduleApi: ScheduleApi = httpClient.create()

    fun createOrUpdate(taskInstance: TaskInstance) {
        val response = scheduleApi.createOrUpdate(taskInstance).execute()
        if (!response.isSuccessful) throw HttpException(response)
    }

    fun setStatus(businessId: String, status: Int) {
        val response = scheduleApi.setStatus(businessId, status).execute()
        if (!response.isSuccessful) throw HttpException(response)
    }

}