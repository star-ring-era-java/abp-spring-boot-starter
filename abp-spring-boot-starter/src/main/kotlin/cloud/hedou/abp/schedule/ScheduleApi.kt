package cloud.hedou.abp.schedule

import cloud.hedou.abp.remote.Api
import retrofit2.Call
import retrofit2.http.*

@Api(41122)
interface ScheduleApi {

    @POST("api/app/task-instance/create-or-update")
    fun createOrUpdate(@Body payload: TaskInstance): Call<TaskInstance>

    @PUT("api/app/task-instance/{sourceTaskInstanceId}/status")
    fun setStatus(
        @Path("sourceTaskInstanceId") businessId: String,
        @Query("status") status: Int
    ): Call<Unit>

}