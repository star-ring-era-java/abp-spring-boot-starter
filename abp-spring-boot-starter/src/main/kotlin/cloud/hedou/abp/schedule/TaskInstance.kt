package cloud.hedou.abp.schedule

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

@NoArguments
data class TaskInstance(

    /** ID */
    var id: String?,

    /** 名称 */
    var title: String,

    /** 任务描述 */
    @get:JsonProperty("remarks")
    @set:JsonAlias("remarks")
    var description: String?,

    /**
     * 状态
     * 0->未创建
     * 1->待执行
     * 2->执行中
     * 3->执行结束
     */
    var status: Int,

    /** 计划中的开始时间 */
    var startTime: LocalDateTime,

    /** 计划中的结束时间 */
    var endTime: LocalDateTime?,

    /** 实际的开始时间，在开始时赋值 */
    var realStartTime: LocalDateTime?,

    /** 实际的结束时间，在结束时赋值 */
    var realEndTime: LocalDateTime?,

    /** 业务实例ID */
    @get:JsonProperty("sourceTaskInstanceId")
    @set:JsonAlias("sourceTaskInstanceId")
    var businessId: String,

    /**
     * 业务类型
     * 1->现场作业
     * 2->日常维保
     * 3->计划性维保
     * 4->审批流程
     * 5->审批任务
     * 6->审批流程v2
     * 7->审批任务v2
     */
    @get:JsonProperty("sourceType")
    @set:JsonAlias("sourceType")
    var businessType: Int,

    /** 可处理任务的用户ID */
    var userIds: List<String>,

    /** 额外数据 */
    var extraProperties: Map<String, Any>,

    )