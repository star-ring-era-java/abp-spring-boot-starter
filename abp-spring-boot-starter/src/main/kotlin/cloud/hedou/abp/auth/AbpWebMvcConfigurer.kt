package cloud.hedou.abp.auth

import cloud.hedou.abp.identity.RemoteIdentityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

/** 将abp的参数解析器添加到配置中 */
@Configuration
class AbpWebMvcConfigurer : WebMvcConfigurer {

    @Autowired
    private lateinit var remoteIdentityService: RemoteIdentityService

    override fun addArgumentResolvers(resolvers: MutableList<HandlerMethodArgumentResolver>) {
        resolvers.add(AbpAuthArgumentResolver(remoteIdentityService))
    }

}