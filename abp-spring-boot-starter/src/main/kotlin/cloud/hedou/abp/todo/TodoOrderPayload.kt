package cloud.hedou.abp.todo

import com.fasterxml.jackson.annotation.JsonProperty

data class TodoOrderPayload(

    val todoItemId: String,

    val orderCode: String?,

    @get:JsonProperty(value = "orderId")
    val processInstanceId: String

)