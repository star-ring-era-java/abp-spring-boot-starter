package cloud.hedou.abp.todo

import com.fasterxml.jackson.annotation.JsonProperty

data class TodoCompletePayload(

    @get:JsonProperty(value = "id")
    val todoId: String,

    @get:JsonProperty(value = "orderId")
    val processInstanceId: String,

    val orderCode: String?,

    val remark: String?

)