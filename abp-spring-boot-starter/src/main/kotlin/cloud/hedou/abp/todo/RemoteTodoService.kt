package cloud.hedou.abp.todo

import cloud.hedou.abp.remote.HttpClient
import cloud.hedou.abp.remote.HttpClient.Companion.create
import retrofit2.HttpException

class RemoteTodoService(httpClient: HttpClient) {

    private val todoApi: TodoApi = httpClient.create()

    fun bindTodo(todoId: String, orderCode: String?, processInstanceId: String) {
        val payload = TodoOrderPayload(todoId, orderCode, processInstanceId)
        val response = todoApi.bindTodo(payload).execute()
        if (!response.isSuccessful) {
            throw HttpException(response)
        }
    }

    fun sendEndEvent(businessKey: String, processInstanceId: String, orderCode: String?, remark: String?) {
        val payload = TodoCompletePayload(businessKey, processInstanceId, orderCode, remark)
        val response = todoApi.sendEndEvent(payload).execute()
        if (!response.isSuccessful) {
            throw HttpException(response)
        }
    }

}