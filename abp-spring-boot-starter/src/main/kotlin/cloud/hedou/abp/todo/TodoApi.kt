package cloud.hedou.abp.todo

import cloud.hedou.abp.remote.Api
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

@Api(44307)
interface TodoApi {

    @POST("api/todo/items/createOrder")
    fun bindTodo(@Body payload: TodoOrderPayload): Call<Unit>

    @POST("api/todo/items/complete")
    fun sendEndEvent(@Body payload: TodoCompletePayload): Call<Unit>

}