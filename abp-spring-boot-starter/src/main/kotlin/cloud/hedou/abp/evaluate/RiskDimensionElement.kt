package cloud.hedou.abp.evaluate

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@NoArguments
data class RiskDimensionElement(

    /** 维度评分元素 */
    @set:JsonAlias("description")
    @get:JsonProperty("description")
    var name: String,

    /** 维度得分 */
    @set:JsonAlias("scope")
    @get:JsonProperty("scope")
    var score: Int

) : Serializable {

    companion object {
        private const val serialVersionUID: Long = -7875271392064373758L
    }
}