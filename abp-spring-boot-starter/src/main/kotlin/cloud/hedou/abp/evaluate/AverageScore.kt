package cloud.hedou.abp.evaluate

import cloud.hedou.annotation.NoArguments
import java.io.Serializable

@NoArguments
data class AverageScore(

    /** 平均分 */
    var avgScope: Double,

    /** 故障模式ID */
    var riskAssessmentId: String,

    /** 提示色 */
    var riskRankColor: String,

    /** 风险等级 */
    var riskRankName: String,

    /** 风险可接受度 */
    var riskRankAcceptability: String,

    /** 风险建议措施 */
    var riskRankMeasure: String,

): Serializable {

    companion object {

        private const val serialVersionUID: Long = 6245373985453197772L

    }
}