package cloud.hedou.abp.evaluate

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@NoArguments
data class RiskResult(

    var id: String,

    /** 风险评估策略ID */
    var riskAssessmentId: String,

    /** 风险评估策略名称 */
    var riskAssessmentName: String,

    /** 评估结果分 */
    @set:JsonAlias("scope")
    @get:JsonProperty("scope")
    var score: Double,

    /** 风险等级颜色 */
    var riskRankColor: String?,

    /** 风险等级名称 */
    var riskRankName: String?

) : Serializable {

    companion object {
        private const val serialVersionUID: Long = -3607069280136911630L
    }

}