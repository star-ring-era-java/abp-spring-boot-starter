package cloud.hedou.abp.evaluate

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@NoArguments
data class RiskDimensionScore(

    /** 风险维度ID */
    @set:JsonAlias("riskLatidudeId")
    @get:JsonProperty("riskLatidudeId")
    var riskDimensionId: String,

    /** 风险维度名称 */
    var riskDimensionName: String,

    /** 最大分 */
    var maxScore: Int,

    /** 分数 */
    @set:JsonAlias("scope", "score")
    @get:JsonProperty("scope")
    var score: Int?,

    /** 备注 */
    @set:JsonAlias("description")
    @get:JsonProperty("description")
    var remark: String?,

): Serializable {

    companion object {
        private const val serialVersionUID: Long = 2908633087175292959L
    }

}