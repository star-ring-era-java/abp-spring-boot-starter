package cloud.hedou.abp.evaluate

import com.fasterxml.jackson.annotation.JsonProperty

data class RPNPayload(

    @get:JsonProperty("orderId")
    val processInstanceId: String,

    val businessTypeNames: List<String>

)