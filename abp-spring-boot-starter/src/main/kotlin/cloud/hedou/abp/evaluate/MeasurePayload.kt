package cloud.hedou.abp.evaluate

import com.fasterxml.jackson.annotation.JsonProperty

data class MeasurePayload(

    @get:JsonProperty("orderId")
    val processInstanceId: String,

    val measure: String,

    @get:JsonProperty(value= "isTriggerCC")
    val isTriggerCC: Boolean,

    @get:JsonProperty(value = "isTriggerCAPA")
    val isTriggerCAPA: Boolean

)