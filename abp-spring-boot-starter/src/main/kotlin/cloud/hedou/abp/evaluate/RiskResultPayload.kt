package cloud.hedou.abp.evaluate

import com.fasterxml.jackson.annotation.JsonProperty

data class RiskResultPayload(

    /** 请求评估风险的业务类型 */
    @get:JsonProperty("businessTypeName")
    val businessType: String,

    /** 请求评估风险的业务ID */
    @get:JsonProperty("businessDataId")
    val businessKey: String,

    /** 使用的风险评估策略ID */
    val riskAssessmentId: String,

    /** 风险等级名称 */
    @get:JsonProperty("items")
    val scores: List<RiskDimensionScore>

)