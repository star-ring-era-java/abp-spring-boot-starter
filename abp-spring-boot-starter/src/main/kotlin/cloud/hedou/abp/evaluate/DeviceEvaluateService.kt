package cloud.hedou.abp.evaluate

import cloud.hedou.abp.remote.HttpClient
import cloud.hedou.abp.remote.HttpClient.Companion.create
import org.springframework.stereotype.Service
import retrofit2.HttpException
import java.time.LocalDateTime

@Service
class DeviceEvaluateService(httpClient: HttpClient) {

    private val deviceEvaluateApi: DeviceEvaluateApi = httpClient.create()

    fun submitRPN(processInstanceId: String, businessTypeNames: List<String>) {
        val rpnPayload = RPNPayload(processInstanceId, businessTypeNames)
        val response = deviceEvaluateApi.submitRPN(rpnPayload).execute()
        if (!response.isSuccessful) throw HttpException(response)
    }

    fun submitMeasure(processInstanceId: String, measure: String, isTriggerCC: Boolean, isTriggerCAPA: Boolean) {
        val measurePayload = MeasurePayload(processInstanceId, measure, isTriggerCC, isTriggerCAPA)
        val response = deviceEvaluateApi.submitMeasure(measurePayload).execute()
        if (!response.isSuccessful) throw HttpException(response)
    }

    fun getAverageScore(processInstanceId: String, businessTypeName: String): AverageScore {
        val response = deviceEvaluateApi.getAverageScore(processInstanceId, businessTypeName).execute()
        if (!response.isSuccessful) throw HttpException(response)
        return response.body()!!
    }

    fun sendEndEvent(payload: Map<String, Any>) {
        val response = deviceEvaluateApi.sendEndEvent(payload).execute()
        if (!response.isSuccessful) throw HttpException(response)
    }

    fun setFaultEvaluationExecutionOrder(businessKey: String, processInstanceId: String, orderCode: String) {
        val payload = mapOf(
            "id" to businessKey,
            "correspondOrderId" to processInstanceId,
            "correspondOrderCode" to orderCode
        )
        val response = deviceEvaluateApi.setFaultEvaluationExecutionOrder(payload).execute()
        if (!response.isSuccessful) throw HttpException(response)
    }

    fun completeFaultEvaluationExecutionOrder(businessKey: String, completeDateTime: LocalDateTime) {
        val payload = mapOf(
            "id" to businessKey,
            "completeDateTime" to completeDateTime
        )
        val response = deviceEvaluateApi.completeFaultEvaluationExecutionOrder(payload).execute()
        if (!response.isSuccessful) throw HttpException(response)
    }

    /** 根据评估规则获取评估维度，并设置为可打分的实体列表 */
    fun getRiskDimensionScores(riskAssessmentId: String): List<RiskDimensionScore> {
        val response = deviceEvaluateApi.getRiskDimensions(riskAssessmentId).execute()
        if (!response.isSuccessful) throw HttpException(response)
        return response.body()!!.map {
            RiskDimensionScore(
                riskDimensionId = it.id,
                riskDimensionName = it.name,
                maxScore = it.range?.size ?: 5,
                score = null,
                remark = null
            )
        }
    }

    /** 提交风险评估的分数，并得到风险结果 */
    fun submitRiskScores(
        businessType: String,
        businessKey: String,
        riskAssessmentId: String,
        scores: List<RiskDimensionScore>,
    ) {
        val response = deviceEvaluateApi.submitRiskScores(
            RiskResultPayload(
                businessType = businessType,
                businessKey = businessKey,
                riskAssessmentId = riskAssessmentId,
                scores = scores
            )
        ).execute()
        if (!response.isSuccessful) throw HttpException(response)
    }

}