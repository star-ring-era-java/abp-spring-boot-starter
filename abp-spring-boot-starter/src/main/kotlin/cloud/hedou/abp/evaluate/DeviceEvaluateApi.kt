package cloud.hedou.abp.evaluate

import cloud.hedou.abp.remote.Api
import retrofit2.Call
import retrofit2.http.*

@Api(44330)
interface DeviceEvaluateApi {

    @POST("api/device-management/device-fault-review/submit-rpn")
    fun submitRPN(@Body payload: RPNPayload): Call<Unit>

    @POST("api/device-management/device-fault-review/submit-measure")
    fun submitMeasure(@Body payload: MeasurePayload): Call<Unit>

    @GET("api/device-management/risk-assessment-result/avg-by-business-data-id/{businessDataId}")
    fun getAverageScore(
        @Path("businessDataId") businessDataId: String,
        @Query("busunessTypeName") businessTypeName: String
    ): Call<AverageScore>

    @POST("api/device-management/device-mainteabce-work-order/workflow-callback")
    fun sendEndEvent(@Body payload: Map<String, @JvmSuppressWildcards Any>): Call<Unit>

    @POST("api/device-management/fault-evaluation/set-fault-evaluation-execution-order")
    fun setFaultEvaluationExecutionOrder(@Body payload: Map<String, @JvmSuppressWildcards Any>): Call<Unit>

    @POST("api/device-management/fault-evaluation/complete-fault-evaluation-execution-order")
    fun completeFaultEvaluationExecutionOrder(@Body payload: Map<String, @JvmSuppressWildcards Any>): Call<Unit>

    @GET("api/device-management/risk-latitude/by-risk-assessment-id/{riskAssessmentId}")
    fun getRiskDimensions(@Path("riskAssessmentId") riskAssessmentId: String ): Call<List<RiskDimension>>

    @POST("api/device-management/risk-assessment-result")
    fun submitRiskScores(@Body payload: RiskResultPayload): Call<RiskResult>

}