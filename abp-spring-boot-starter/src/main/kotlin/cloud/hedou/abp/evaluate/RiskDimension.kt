package cloud.hedou.abp.evaluate

import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@NoArguments
data class RiskDimension(

    /** 风险维度ID */
    var id: String,

    /** 风险维度名称 */
    @set:JsonAlias("name")
    @get:JsonProperty("name")
    var key: String,

    /** 风险维度完整名称 */
    @set:JsonAlias("fullName")
    @get:JsonProperty("fullName")
    var name: String,

    /** 风险维度描述 */
    var description: String?,

    /** 所属的风险策略 */
    var riskAssessmentId: String,

): Serializable {

    @set:JsonAlias("riskLatitudeScopes")
    @get:JsonProperty("riskLatitudeScopes")
    var range: List<RiskDimensionElement>? = null

    companion object {
        private const val serialVersionUID: Long = -1022223560462843200L
    }

}