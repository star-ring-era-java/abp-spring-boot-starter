package cloud.hedou.abp.workorder

import cloud.hedou.abp.remote.HttpClient
import cloud.hedou.abp.remote.HttpClient.Companion.create
import retrofit2.HttpException

class RemoteWorkOrderService(httpClient: HttpClient) {

    private val workOrderApi: WorkOrderApi = httpClient.create()

    fun faultAnalysisCallback(payload: Map<String, Any>) {
        val response = workOrderApi.faultAnalysisCallback(payload).execute()
        if (!response.isSuccessful) {
            throw HttpException(response)
        }
    }

}