package cloud.hedou.abp.workorder

import cloud.hedou.abp.remote.Api
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

@Api(44341)
interface WorkOrderApi {

    @POST("api/WorkOrder/ordering/fault-analysis-workflow-callback")
    fun faultAnalysisCallback(@Body payload: Map<String, @JvmSuppressWildcards Any>): Call<Unit>

}