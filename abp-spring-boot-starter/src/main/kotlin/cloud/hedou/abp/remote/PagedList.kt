package cloud.hedou.abp.remote

import cloud.hedou.annotation.NoArguments

@NoArguments
data class PagedList<T>(

    var totalCount: Int,

    var items: List<T>

)