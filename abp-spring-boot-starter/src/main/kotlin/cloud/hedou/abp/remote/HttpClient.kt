package cloud.hedou.abp.remote

interface HttpClient {

    fun <T> create(klass: Class<T>): T

    companion object {

        inline fun <reified T> HttpClient.create(): T = create(T::class.java)

    }

}