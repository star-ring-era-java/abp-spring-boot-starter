package cloud.hedou.abp.remote

import cloud.hedou.annotation.NoArguments

@NoArguments
data class UserConfiguration(

    val auth: Authorization

)