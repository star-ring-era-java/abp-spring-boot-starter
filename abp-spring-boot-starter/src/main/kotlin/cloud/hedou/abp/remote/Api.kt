package cloud.hedou.abp.remote

/** 表示接口是Retrofit Api */
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class Api(

    /** 该API的访问端口 */
    val port: Int

)
