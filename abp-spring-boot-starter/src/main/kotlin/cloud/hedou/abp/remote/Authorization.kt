package cloud.hedou.abp.remote

import cloud.hedou.annotation.NoArguments

@NoArguments
data class Authorization(

    val grantedPolicies: Map<String, Boolean>

)