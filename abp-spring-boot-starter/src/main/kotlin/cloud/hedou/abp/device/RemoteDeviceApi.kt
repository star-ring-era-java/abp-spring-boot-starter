package cloud.hedou.abp.device

import cloud.hedou.abp.remote.Api
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

@Api(44313)
interface RemoteDeviceApi {

    @GET("api/equipment-management/device/{id}")
    fun getDevice(@Path("id") deviceId: String): Call<Device>

    @PUT("api/equipment-management/device/{id}")
    fun setDevice(
        @Path("id") deviceId: String,
        @Body payload: Device
    ): Call<Unit>

}