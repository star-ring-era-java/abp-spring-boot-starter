package cloud.hedou.abp.device

import cloud.hedou.annotation.Identifiable
import cloud.hedou.annotation.NoArguments
import java.io.Serializable

@NoArguments
data class Device(

    @Identifiable
    var id: String,

    /** 设备名称 */
    var name: String?,

    /** 系统生成的编码 */
    @Identifiable
    var code: String?,

    /** 描述 */
    var description: String?,

    /** 资产Id */
    var assetId: String?,

    /** 管理部门ID */
    var manageOrganizationUnitId: String?,

    /** 设备状态分组ID */
    var deviceGroupId: String?,

    /** 设备状态ID */
    var deviceStatuId: String?,

    /** 设备状态名称 */
    var status: String?,

    /** 序列码 */
    @Identifiable
    var sequence: String?,

    /** 设备型号、规格 */
    var specification: String?,

    /** 是否是重要的设备 */
    var crucial: Boolean?,

    /** 是否锁定，锁定后不允许删除 */
    var isLocked: Boolean?,

    /** 预览图 */
    var image: String?,

    /** 正视图 */
    var image1: String?,

    /** 侧视图 */
    var image2: String?,

    /** 俯视图 */
    var image3: String?,

    /** 设备模型ID */
    var fromModelInstanceId: String?,

    /** 经度 */
    var longitude: String?,

    /** 纬度 */
    var latitude: String?,

    /** 所属系统 */
    var systemType: String?,

    /** 所属系统名称 */
    var systemTypeName: String?

) : Serializable {

    companion object {
        private const val serialVersionUID: Long = -4451135465773932742L
    }

}