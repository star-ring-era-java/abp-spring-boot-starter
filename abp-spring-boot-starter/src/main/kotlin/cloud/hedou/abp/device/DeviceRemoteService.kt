package cloud.hedou.abp.device

import cloud.hedou.abp.remote.HttpClient
import cloud.hedou.abp.remote.HttpClient.Companion.create
import retrofit2.HttpException

class DeviceRemoteService(httpClient: HttpClient) {

    private val deviceApi: RemoteDeviceApi = httpClient.create()

    fun getDevice(deviceId: String): Device {
        val response = deviceApi.getDevice(deviceId).execute()
        if(!response.isSuccessful) throw HttpException(response)
        return response.body()!!
    }

    fun setDevice(device: Device) {
        val response = deviceApi.setDevice(device.id, device).execute()
        if (!response.isSuccessful) throw HttpException(response)
    }

}