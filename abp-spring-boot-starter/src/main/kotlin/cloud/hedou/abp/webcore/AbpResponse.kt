package cloud.hedou.abp.webcore

/** 适配abp服务端的响应体结构 */
data class AbpResponse(val code: Int, val error: ErrorMessage)