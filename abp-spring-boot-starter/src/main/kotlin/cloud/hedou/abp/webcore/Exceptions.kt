package cloud.hedou.abp.webcore

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class BadRequestException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)

@ResponseStatus(HttpStatus.FORBIDDEN)
class ForbiddenException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)

@ResponseStatus(HttpStatus.UNAUTHORIZED)
class UnauthorizedException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
class InternalServerException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)

@ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
class NotImplementedException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)