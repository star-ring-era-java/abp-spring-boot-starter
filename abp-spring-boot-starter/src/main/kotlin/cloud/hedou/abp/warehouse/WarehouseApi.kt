package cloud.hedou.abp.warehouse

import cloud.hedou.abp.remote.Api
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

@Api(21035)
interface WarehouseApi {

    @POST("api/app/abp-flowable/workflow-callback")
    fun sendProcessInstanceEvent(@Body payload: Map<String, @JvmSuppressWildcards Any>): Call<Unit?>

    @POST("api/app/apply-plan/workflow-callback")
    fun sendSuppliesMonthlyPlanApprovalResult(@Body payload: Map<String, @JvmSuppressWildcards Any>): Call<Unit?>

}