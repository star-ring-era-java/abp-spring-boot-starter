package cloud.hedou.abp.warehouse

import cloud.hedou.abp.remote.HttpClient
import cloud.hedou.abp.remote.HttpClient.Companion.create
import org.springframework.stereotype.Service
import retrofit2.HttpException

@Service
class RemoteWarehouseService(httpClient: HttpClient) {

    private val warehouseApi: WarehouseApi = httpClient.create()

    fun sendProcessInstanceEvent(payload: Map<String, Any>) {
        val response = warehouseApi.sendProcessInstanceEvent(payload).execute()
        if (!response.isSuccessful) {
            throw HttpException(response)
        }
    }

    fun sendSuppliesMonthlyPlanApprovalResult(payload: Map<String, Any>) {
        val response = warehouseApi.sendSuppliesMonthlyPlanApprovalResult(payload).execute()
        if (!response.isSuccessful) {
            throw HttpException(response)
        }
    }

}