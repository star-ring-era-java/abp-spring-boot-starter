package cloud.hedou.abp.starter

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "abp.server")
class Server(

    /** 默认服务器IP */
    var url: String? = null,

    /** 授权服务器IP */
    var oauth: String? = null,

    /** 基础数据服务器IP */
    var commonData: String? = null,

    /** 消息通知服务器IP */
    var notify: String? = null,

    /** 日程服务器IP */
    var schedule: String? = null,

    /** 备品备件服务器IP */
    var spareParts: String? = null,

    /** 工单故障分析服务器IP */
    var workOrder: String? = null,

    /** 更多端口转发路径 */
    var forwards: Map<Int, String>? = null,

    ) {

    fun toMap(): Map<Int, String> {
        return buildMap {
            if (!oauth.isNullOrEmpty()) put(44318, oauth!!)
            if (!notify.isNullOrEmpty()) put(44386, notify!!)
            if (!schedule.isNullOrEmpty()) put(41122, schedule!!)
            if (!workOrder.isNullOrEmpty()) put(44341, workOrder!!)
            if (!spareParts.isNullOrEmpty()) put(21035, spareParts!!)
            if (!commonData.isNullOrEmpty()) put(44328, commonData!!)
            if (!forwards.isNullOrEmpty()) {
                putAll(forwards!!)
            }
        }
    }

}