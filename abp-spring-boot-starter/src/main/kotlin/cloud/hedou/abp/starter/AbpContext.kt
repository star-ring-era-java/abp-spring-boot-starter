package cloud.hedou.abp.starter

import org.springframework.beans.factory.getBean
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware

class AbpContext : ApplicationContextAware {

    override fun setApplicationContext(applicationContext: ApplicationContext) {
        context = applicationContext
    }

    companion object {

        /** 注册后的服务器专用token */
        internal var serverToken: String? = null
            set(value) {
                field = "Bearer $value"
            }

        lateinit var context: ApplicationContext
            private set

        inline fun <reified T> getBean(): T = context.getBean()
    }

}