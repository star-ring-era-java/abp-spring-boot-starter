package cloud.hedou.abp.starter

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "abp.auth")
class Auth(

    var certificate: Map<String, String>? = null,

    var permitUrls: List<String>? = null

)
