package cloud.hedou.abp.asset

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

data class ReturnPayload(

    /** 流程ID */
    @get:JsonProperty("orderId")
    val processInstanceId: String?,

    /** 工单编号 */
    val orderCode: String?,

    /** 归还用户 */
    val userId: String?,

    /** 归还部门 */
    @get:JsonProperty("organizationUnitId")
    val departmentId: String?,

    /** 归还时间 */
    val returnTime: LocalDateTime?,

    /** 归还的资产ID */
    val assetIds: List<String>?

)