package cloud.hedou.abp.asset

import cloud.hedou.abp.common.CommonData
import cloud.hedou.annotation.Identifiable
import cloud.hedou.annotation.NoArguments
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import java.time.LocalDateTime

@NoArguments
data class Asset(

    @Identifiable
    var id: String?,

    /** 资产名称 */
    var name: String?,

    /** 系统编号 */
    @Identifiable
    var code: String?,

    /** 资产编号，企业自编码 */
    @Identifiable
    var assetCode: String,

    /** 设备编号 */
    var deviceCode: String?,

    /** 序列号 */
    @Identifiable
    var sequence: String?,

    /** 型号 */
    var specification: String?,

    /** 资产描述 */
    var description: String?,

    /** 资产分类ID */
    var categoryId: String?,

    /** 启用日期 */
    var enableDate: LocalDateTime?,

    /** 到期日期 */
    var expireDate: LocalDateTime?,

    /** 使用部门 */
    var usageOrganizationUnitId: String?,

    /** 管理部门 */
    var manageOrganizationUnitId: String?,

    /** 归属部门 */
    var belongToOrganizationUnitId: String?,

    /** 是否关键 */
    @set:JsonAlias("isCrucial")
    @get:JsonProperty("isCrucial")
    var isCrucial: Boolean?,

    /** true 自定义 false SAP */
    @set:JsonAlias("isVirtual")
    @get:JsonProperty("isVirtual")
    var isVirtual: Boolean?,

    /** 存放地点 */
    var placeOfStorage: String?,

    /** 经办人 */
    var operatorUser: String?,

    /** 使用人 */
    var usageUser: String?,

    /** 创建时间 */
    var creationTime: LocalDateTime?,

    /** 资产状态 */
    var status: Int,

    /** 产品id */
    var productId: String?,

    /** 是否已入库 */
    @set:JsonAlias("isInStore")
    @get:JsonProperty("isInStore")
    var isInStore: Boolean?,

    /** 资产图片 */
    var fileTokens: String?,

    /** 厂家 */
    var factory: String?,

    /** 机组编码 */
    var unitNumber: String?,

    /** 制造国别 */
    var country: String?,

    /** 供应商 */
    var supplier: String?,

    /** 报废ID */
    var assetScrapId: String?,

    /** 标签 */
    var tags: List<String>?,

    /** 类别 */
    var category: CommonData?,

    /** 资产自有额外属性 */
    var extraProperties: List<AssetExtraProperty>?

) : Serializable {

    /** 使用部门 */
    val usageDepartmentId: String?
        get() = usageOrganizationUnitId

    /** 管理部门 */
    val manageDepartmentId: String?
        get() = manageOrganizationUnitId

    /** 归属部门 */
    val belongToDepartmentId: String?
        get() = belongToOrganizationUnitId

    companion object {

        private const val serialVersionUID: Long = 8810322087399658086L
    }

}

