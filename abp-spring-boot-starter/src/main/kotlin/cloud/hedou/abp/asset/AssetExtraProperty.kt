package cloud.hedou.abp.asset

import cloud.hedou.annotation.Identifiable
import cloud.hedou.annotation.NoArguments
import java.io.Serializable

@NoArguments
data class AssetExtraProperty(

    /** 字段名 */
    @Identifiable
    var fieldName: String?,

    /** 值 */
    var value: String?
    
) : Serializable {

    companion object {

        private const val serialVersionUID: Long = 6310672565856274640L

    }

}