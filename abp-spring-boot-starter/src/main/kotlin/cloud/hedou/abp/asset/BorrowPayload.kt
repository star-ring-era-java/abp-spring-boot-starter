package cloud.hedou.abp.asset

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

data class BorrowPayload(

    /** 流程ID */
    @get:JsonProperty("orderId")
    val processInstanceId: String?,

    /** 业务类型 */
    val businessType: String?,

    /** 业务ID */
    val businessId: String?,

    /** 业务名称 */
    val businessName: String?,

    /** 工单编号 */
    val orderCode: String?,

    /** 申请用户 */
    val userId: String?,

    /** 申请部门 */
    @get:JsonProperty("organizationUnitId")
    val departmentId: String?,

    /** 借出时间 */
    val borrowTime: LocalDateTime?,

    /** 计划归还时间 */
    val planReturnTime: LocalDateTime?,

    /** 借的资产数据ID列表 */
    val assetIds: List<String>

)