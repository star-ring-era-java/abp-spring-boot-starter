package cloud.hedou.abp.asset

import cloud.hedou.abp.remote.Api
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

@Api(44024)
interface RemoteAssetApi {

    @GET("api/asset-management/assets/{id}")
    fun getAsset(@Path("id") assetId: String): Call<Asset>

    @PUT("api/asset-management/assets/{id}")
    fun setAsset(@Path("id") assetId: String, @Body asset: Asset): Call<Asset>

    @POST("api/asset-management/borrow-record")
    fun assetBorrow(@Body payload: BorrowPayload): Call<Unit>

    @POST("api/asset-management/return-record")
    fun assetReturn(@Body payload: ReturnPayload): Call<Unit>

}