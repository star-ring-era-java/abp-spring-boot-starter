## abp-spring-boot-starter

### 功能

以`ABP`为主的`String Boot`资源服务器，当`ABP`的功能无法满足业务需求时，可通过`Spring Boot`框架扩展自身业务，以利用市面上充沛的`java`框架。

### 使用手册

1. 在`Spring Boot`项目中引用此模块:

   - maven
     ```xml
       <dependency>
           <groupId>cloud.hedou</groupId>
           <artifactId>abp-spring-boot-starter</artifactId>
           <version>1.0.0</version>
       </dependency>
     ```
   - gradle
     ```groovy
     implementation 'cloud.hedou:abp-spring-boot-starter:1.0.0'  
     ``` 
   - gradle.kts
     ```kotlin
     implementation("cloud.hedou:abp-spring-boot-starter:1.0.0") 
     ```
2. 在`resources`中添加以下配置：
   - yml
     ```yml
     spring:
       rabbitmq:
         host: 192.168.3.250
         port: 5672
         username: admin
         password: admin

     abp:
       server:
        url: http://192.168.3.237
     ```
   
   - properties
     ```properties
     spring.rabbitmq.host=192.168.3.250
     spring.rabbitmq.port=5672
     spring.rabbitmq.username=admin
     spring.rabbitmq.password=admin
   
     abp.server.url=http:\\192.168.3.237
     ```

### 版本更新记录

   * 1.0.1
     - 可从yml中配置需要忽略鉴权的API地址。

   * 1.0.0 
     - 初次发布，兼容`spring-boot`的`2.6.6~2.7.0`版本