plugins {
    kotlin("jvm") version "1.6.20" apply false
    kotlin("plugin.jpa") version "1.6.20" apply false
    kotlin("plugin.noarg") version "1.6.20" apply false
    kotlin("plugin.spring") version "1.6.20" apply false
    id("org.springframework.boot") version "2.6.6" apply false
    id("com.vanniktech.maven.publish") version "0.19.0" apply false
    id("io.spring.dependency-management") version "1.0.11.RELEASE" apply false
}

tasks.register<Delete>("clean") {
    delete(rootProject.buildDir)
    delete("logs")
}
